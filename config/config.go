package config

import (
	"assigment-rest-api/structs"

	"github.com/jinzhu/gorm"
)

// UPDATE DB HERE
var db_username string = "usernameexample"
var db_password string = "passwordexample"
var db_address string = "localhost"
var db_port string = "3306"
var db_name string = "orders_by"

func DBInit() *gorm.DB {
	db, err := gorm.Open("mysql", db_username+":"+db_password+"@tcp("+db_address+":"+db_port+")/"+db_name+"?charset=utf8mb4&parseTime=True&loc=Local")
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(structs.Items{})
	db.AutoMigrate(structs.Orders{})
	return db
}

package controllers

import (
	"assigment-rest-api/structs"
	"net/http"

	"github.com/gin-gonic/gin"
)

type DataResult struct {
	// Order_id int `json:"orderId"`
	structs.Orders
	Items []structs.Items `json:"items"`
}

func (idb *InDB) GetOrder(c *gin.Context) {
	var (
		order  structs.Orders
		items  []structs.Items
		result gin.H
	)
	id := c.Param("id")
	err := idb.DB.Where("order_id = ?", id).First(&order).Error
	if err != nil {
		result = gin.H{
			"result": err.Error(),
			"count":  0,
		}
	} else {
		var data = DataResult{}
		data.Orders = order
		err := idb.DB.Where("order_id = ?", id).Find(&items).Error
		if err != nil {
			data.Items = nil
		} else {
			data.Items = items
		}
		result = gin.H{
			"result": data,
			"count":  1,
		}
	}
	c.JSON(http.StatusOK, result)
}

func (idb *InDB) GetOrders(c *gin.Context) {
	var (
		orders []structs.Orders
		res    []DataResult
		result gin.H
	)

	idb.DB.Find(&orders)
	if len(orders) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {

		for _, order := range orders {
			var data = DataResult{}
			var items = []structs.Items{}
			data.Orders = order
			err := idb.DB.Where("order_id = ?", order.Order_id).Find(&items).Error
			if err != nil {
				data.Items = nil
			} else {
				data.Items = items
			}
			res = append(res, data)
		}
		result = gin.H{
			"result": res,
			"count":  len(orders),
		}
	}

	c.JSON(http.StatusOK, result)
}

func (idb *InDB) CreateOrder(c *gin.Context) {
	var (
		input  DataResult
		order  structs.Orders
		result gin.H
	)
	// Validate input
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	order.Ordered_at = input.Ordered_at
	order.Customer_name = input.Customer_name
	idb.DB.Create(&order)
	new_items := []structs.Items{}
	for _, i := range input.Items {
		i.Order_id = order.Order_id
		idb.DB.Create(&i)
		new_items = append(new_items, i)
	}
	result = gin.H{
		"result": map[string]interface{}{
			"order": order,
			"items": new_items,
		},
	}
	c.JSON(http.StatusOK, result)
}

func (idb *InDB) UpdateOrder(c *gin.Context) {
	var (
		input    DataResult
		order    structs.Orders
		newOrder structs.Orders
		result   gin.H
	)
	// Validate input
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	err := idb.DB.Where("Order_id = ?", input.Order_id).First(&order).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
		c.JSON(http.StatusNotFound, result)
		return
	}
	newOrder.Ordered_at = input.Ordered_at
	newOrder.Customer_name = input.Customer_name
	err = idb.DB.Model(&order).Updates(newOrder).Error
	if err != nil {
		res_msg := "update failed"
		result = gin.H{
			"result": res_msg,
		}
		c.JSON(http.StatusBadRequest, result)
		return
	}
	success_items := []uint64{}
	fail_items := []uint64{}
	for _, i := range input.Items {

		var item = structs.Items{}
		var newItem = structs.Items{}
		err := idb.DB.Where("Order_id = ?", input.Order_id).Where("Item_id = ?", i.Item_id).First(&item).Error
		if err != nil {
			i.Order_id = order.Order_id
			idb.DB.Create(&i)
			success_items = append(success_items, i.Item_id)
			continue
		}
		newItem.Item_code = i.Item_code
		newItem.Description = i.Description
		newItem.Quantity = i.Quantity
		newItem.Order_id = order.Order_id
		err = idb.DB.Model(&item).Updates(newItem).Error
		if err != nil {
			fail_items = append(fail_items, i.Item_id)
		} else {
			success_items = append(success_items, i.Item_id)
		}
	}
	result = gin.H{
		"result":        "successfully updated order " + string(rune(input.Order_id)),
		"fail_items":    fail_items,
		"success_items": success_items,
	}
	c.JSON(http.StatusOK, result)
}

func (idb *InDB) DeleteOrder(c *gin.Context) {
	var (
		order  structs.Orders
		items  []structs.Items
		result gin.H
	)
	order_id := c.Param("id")
	err := idb.DB.Where("Order_id = ?", order_id).First(&order).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
		c.JSON(http.StatusNotFound, result)
		return
	}

	err = idb.DB.Delete(&order).Error
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
		c.JSON(http.StatusBadRequest, result)
		return
	}
	err = idb.DB.Where("Order_id = ?", order_id).Find(&items).Error
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
		c.JSON(http.StatusBadRequest, result)
		return
	}
	for _, i := range items {
		idb.DB.Delete(&i)
	}
	result = gin.H{
		"result": "successfully deleted order " + order_id,
	}
	c.JSON(http.StatusOK, result)
}

package structs

type Items struct {
	Item_id     uint64 `gorm:"primary_key" json:"lineItemId"`
	Item_code   string `json:"itemCode"`
	Description string `json:"description"`
	Quantity    int    `json:"quantity"`
	Order_id    uint64 `gorm:"ForeignKey:Order_id;References:Order_id"`
}

type Orders struct {
	Order_id      uint64 `gorm:"primary_key" json:"orderId"`
	Customer_name string `json:"customerName"`
	Ordered_at    string `json:"orderedAt"`
}

# HACTIV8 GOLANG SECOND ASSIGNMENT
- golang > go1.13.8
- install importan package
> ``` $ go get -u github.com/jinzhu/gorm ```<br/>
> ``` $ go get -u github.com/go-sql-driver/mysql ```<br/>
> ``` $ go get -u github.com/jinzhu/gorm/dialects/mysql ```<br/>
> ``` $ go get -u github.com/gin-gonic/gin ```<br/>

## DATABASE
- set the database username, password, etc at config/config.go

## RUN SERVER
- set the address and port at base.go
- run the server by running python app.py
> ``` $ go run base.go```

## IMPORT POSTMAN
- import existing postman and env for easier testing
> ``` POSTMAN = golangrest.postman_collection.json ```<br/>
> ``` postman env = localtest.postman_environment.json ```
- don't forget to change the url in postman env, the default is http://localhost:3000
package main

import (
	"assigment-rest-api/config"
	"assigment-rest-api/controllers"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db := config.DBInit()
	inDB := &controllers.InDB{DB: db}

	router := gin.Default()

	router.GET("/order/:id", inDB.GetOrder)
	router.GET("/order", inDB.GetOrders)
	router.POST("/order", inDB.CreateOrder)
	router.PUT("/order", inDB.UpdateOrder)
	router.DELETE("/order/:id", inDB.DeleteOrder)
	router.Run(":3000")
}
